package Asignment;

import java.util.*;

public class Assignment10 {

	public static int NhapN(int n) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhập vào N:");
		n = input.nextInt();
		System.out.println("---------------------");
		if (n <= 0) {
			do {
				System.out.println("Vui lòng nhập lại N :");
				n = input.nextInt();
				System.out.println("---------------------");
			} while (n <= 0);
		}
		return n;
	}

	public static int NhapPhanTuX(int x) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhập vào x:");
		x = input.nextInt();
		System.out.println("---------------------");
		return x;
	}

	public static float Bai1(float a, int N) {
		a = 1;
		for (int i = 0; i <= N; i++) {
			a += i * a;
		}
		return a;
	}

	public static float Bai2(float b, int N, int x1) {
		b = 0;
		for (int i = 0; i <= N; i++) {
			b += Math.pow(x1, i) - 1;
		}
		return b;
	}

	public static float Bai3(float c, int N, int x1) {
		c = 0;
		for (int i = 2; i <= N; i += 2) {
			c += Math.pow(x1, i);
		}
		return c;
	}

	public static float Bai4(float d, int N, int x1) {
		d = 0;
		for (int i = 1; i <= N; i += 2) {
			d += Math.pow(x1, i);
		}
		return d;
	}

	public static float Bai5(float e, int N) {
		e = 1;
		for (int i = 0; i <= N; i++) {
			e += (float) i / (e + i);
		}
		return e;
	}

	public static float Bai6(float f, int N, int x1) {
		f = 0;
		for (int i = 1; i <= N; i++) {
			f += Math.pow(x1, i) / (f + i);
		}
		return f;
	}

	public static float Bai7(float g, int N, int x1) {
		g = 1;
		for (int i = 1; i <= N; i++) {
			g += Math.pow(x1, i) / (g * i);
		}
		return g;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 0, x = 0, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0;
		int N = NhapN(n);
		int x1 = NhapPhanTuX(x);
		float A = Bai1(a, N);
		float B = Bai2(b, N, x1);
		float C = Bai3(c, N, x1);
		float D = Bai4(d, N, x1);
		float E = Bai5(e, N);
		float F = Bai6(f, N, x1);
		float G = Bai7(g, N, x1);
		System.out.println("Kết quả dãy 1 là: " + A);
		System.out.println("Kết quả dãy 2 là: " + B);
		System.out.println("Kết quả dãy 3 là: " + C);
		System.out.println("Kết quả dãy 4 là: " + D);
		System.out.println("Kết quả dãy 5 là: " + E);
		System.out.println("Kết quả dãy 6 là: " + F);
		System.out.println("Kết quả dãy 7 là: " + G);
	}

}
