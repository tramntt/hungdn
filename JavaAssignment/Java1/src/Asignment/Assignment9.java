package Asignment;

import java.util.*;

public class Assignment9 {

	public static int NhapN(int n) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhập vào N:");
		n = input.nextInt();
		System.out.println("---------------------");
		if (n <= 0) {
			do {
				System.out.println("Vui lòng nhập lại N :");
				n = input.nextInt();
				System.out.println("---------------------");
			} while (n <= 0);
		}
		return n;
	}

	public static float Bai1(float a, int N) {
		a = 0;
		for (int i = 1; i <= N; i++) {
			a += i;
		}
		return a;
	}

	public static float Bai2(float b, int N) {
		b = 0;
		for (int i = 1; i <= N; i++) {
			b += Math.pow(i, 2);
		}
		return b;
	}

	public static float Bai3(float c, int N) {
		c = 1;
		for (int i = 1; i <= N; i++) {
			c += 1.0 / (i + 1);
		}
		return c;
	}

	public static float Bai4(float d, int N) {
		d = 0;
		for (int i = 0; i <= N; i += 2) {
			d += 1.0 / (i + 2);
		}
		return d;
	}

	public static float Bai5(float e, int N) {
		e = 1;
		for (int i = 0; i <= N; i += 2) {
			e += 1.0 / (i + 3);
		}
		return e;
	}

	public static float Bai6(float f, int N) {
		f = 0;
		for (int i = 1; i <= N; i++) {
			f += 1.0 / (i * (i + 1));
		}
		return f;
	}

	public static float Bai7(float g, int N) {
		g = 0;
		for (int i = 1; i <= N; i++) {
			g += (float) i / (i + 1);
		}
		return g;
	}

	public static float Bai8(float h, int N) {
		h = 0;
		for (int i = 1; i <= N; i += 2) {
			h += (float) i / (i + 1);
		}
		return h;
	}

	public static float Bai9(float l, int N) {
		l = 1;
		for (int i = 1; i < N; i++) {
			l *= i;
		}
		return l;
	}

	public static float Bai10(float k, int N) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhập vào x:");
		int x = input.nextInt();
		k = 0;
		for (int i = 1; i < N; i++) {
			k += (float) Math.pow(x, N);
		}
		return k;
	}

	public static void main(String[] agrs) {
		int n = 0, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0, l = 0, k = 0;
		int N = NhapN(n);
		float A = Bai1(a, N);
		float B = Bai2(b, N);
		float C = Bai3(c, N);
		float D = Bai4(d, N);
		float E = Bai5(e, N);
		float F = Bai6(f, N);
		float G = Bai7(g, N);
		float H = Bai8(h, N);
		float L = Bai9(l, N);
		float K = Bai10(k, N);
		System.out.println("---------------------");
		System.out.println("Kết quả dãy 1 là: " + A);
		System.out.println("Kết quả dãy 2 là: " + B);
		System.out.println("Kết quả dãy 3 là: " + C);
		System.out.println("Kết quả dãy 4 là: " + D);
		System.out.println("Kết quả dãy 5 là: " + E);
		System.out.println("Kết quả dãy 6 là: " + F);
		System.out.println("Kết quả dãy 7 là: " + G);
		System.out.println("Kết quả dãy 8 là: " + H);
		System.out.println("Kết quả dãy 9 là: " + L);
		System.out.println("Kết quả dãy 10 là: " + K);
	}
}
