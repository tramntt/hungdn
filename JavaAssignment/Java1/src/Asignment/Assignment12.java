package Asignment;

import java.util.*;

public class Assignment12 {

	public static int NhapSoPhanTuMangNguyen(int n) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap n:");
		n = input.nextInt();
		return n;
	}

	public static int[] NhapMangNguyen(int N) {
		Scanner input = new Scanner(System.in);
		int[] a = new int[N];
		System.out.println("Nhap mang a: ");
		for (int i = 0; i < N; i++) {
			a[i] = input.nextInt();
		}
		for (int i = 0; i < N; i++) {
			System.out.println("a[" + i + "]" + a[i]);
		}
		return a;
	}

	public static int NhapSoPhanTuMangThuc(int m) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap m:");
		m = input.nextInt();
		return m;
	}

	public static float[] NhapMangThuc(int M) {
		Scanner input = new Scanner(System.in);
		float[] b = new float[M];
		System.out.println("Nhap mang b: ");
		for (int i = 0; i < M; i++) {
			b[i] = input.nextFloat();
		}
		for (int i = 0; i < M; i++) {
			System.out.println("b[" + i + "]" + b[i]);
		}
		return b;
	}

	public static int NhapX(int x) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap x:");
		x = input.nextInt();
		return x;
	}

	public static int NhapY(int y, int x1) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhap Y:");
		y = input.nextInt();
		do {
			if (y <= x1) {
				System.out.println("Moi nhap lai: ");
				y = input.nextInt();
			}
		} while (y <= x1);
		return y;
	}

//Bai 1
	public static void SoAmMangThuc(float[] b1, int M) {
		System.out.println("cac so am mang so thuc la:");
		for (int i = 0; i < M; i++) {
			if (b1[i] < 0) {
				System.out.println("b1[" + i + "]" + b1[i]);
			}
		}
	}

//Bai 2
	public static void SoThucDoanXY(float[] b1, int M, int x1, int y1) {
		System.out.println("cac so thuc thuoc doan xy la: ");
		for (int i = 0; i < M; i++) {
			if (x1 <= b1[i] && b1[i] <= y1) {
				System.out.println("b1[" + i + "]" + b1[i]);
			}
		}
	}

//Bai 3
	public static void SoNguyenChanDoanXY(int[] a1, int N, int x1, int y1) {
		System.out.println("cac so nguyen chan doan xy la:");
		for (int i = 0; i < N; i++) {
			if (a1[i] % 2 == 0) {
				if (a1[i] >= x1 && a1[i] <= y1) {
					System.out.println("a1[" + i + "]" + a1[i]);
				}
			}
		}
	}

//Bai 4
	public static void ASS4(int[] a1, int N) {
		System.out.println("Cac so thoa Ass4 la: ");
		int bp1=0;
			for (int i = 0; i < N; i++) {
				if((i+1)<N) {
					bp1 = Math.abs(a1[i+1]);
				}
				if (a1[i] > bp1) {
					System.out.println("a1[" + i + "]" + a1[i]);
				}
			}
	}

//Bai5
	public static void ASS5(int[] a1, int N) {
		System.out.println("cac so thoa Ass5 la: ");
		int bp2=0;
		int bp3=0;
		for (int i = 0; i < N; i++) {
			if((i-1)>=0) {
				bp2 = Math.abs(a1[i-1]);
			}
			if((i+1)<N) {
				bp3 = Math.abs(a1[i+1]);
			}
			if (bp2 < a1[i] && a1[i] < bp3) {
				System.out.println("a1[" + i + "]" + a1[i]);
			}
		}
	}

//Bai6
	public static void TongPhanTuMangNguyen(int[] a1, int N) {
		int T1 = 0;
		for (int i = 0; i < N; i++) {
			T1 += a1[i];
		}
		System.out.println("tong gia tri mang nguyen la: " + T1);
	}

	public static void TongPhanTuMangThuc(float[] b1, int M) {
		int T2 = 0;
		for (int i = 0; i < M; i++) {
			T2 += b1[i];
		}
		System.out.println("Tong gia tri mang thuc la: " + T2);
	}

//Bai7
	public static int KiemTraSoKetThucVoiSoLe(int N) {
		int M;
		N = Math.abs(N);
		while (N >= 10) {
			M = N % 10;
			N /= 10;
		}
		if (N % 2 == 0)
			return 0;
		return 1;
	}

	public static void TongCacSoNguyenLe(int[] a1, int N) {
		int T3 = 0;
		for (int i = 0; i < N; i++) {
			if (KiemTraSoKetThucVoiSoLe(a1[i]) == 1) {
				T3 += a1[i];
			}
		}
		System.out.println("tong cac so nguyen bat dau bang so le la: " + T3);
	}

//Bai8
	public static int HangChucLa5(int N) {
		N = Math.abs(N);
		N = N / 10;
		int KT5 = N % 10;
		if (KT5 == 5)
			return 1;
		return 0;
	}

	public static void TongCacSoHangChucLa5(int[] a1, int N) {
		int T4 = 0;
		for (int i = 0; i < N; i++) {
			if (HangChucLa5(a1[i]) == 1) {
				T4 += a1[i];
			}
		}
		System.out.println("tong cac so ket thuc bang 5 la: " + T4);
	}

//Bai9
	public static void SoCacSoChan(int[] a1, int N) {
		int P = 0;
		for (int i = 0; i < N; i++) {
			if (a1[i] % 2 == 0) {
				P++;
			}
		}
		System.out.println("So luong cac so chan la: " + P);
	}

//Bai10
	public static void SoDuongChiaHet7(int[] a1, int N) {
		int Q = 0;
		for (int i = 0; i < N; i++) {
			if (a1[i] > 0 && a1[i] % 7 == 0) {
				Q++;
			}
		}
		System.out.println("So duong chia het cho 7 la: " + Q);
	}

//Bai11
	public static void SoPhanTuX(int[] a1, int N) {
		Scanner input = new Scanner(System.in);
		int K = 0;
		int X = input.nextInt();
		for (int i = 0; i < N; i++) {
			if (a1[i] == X) {
				K++;
			}
		}
		System.out.println("So phan tu x la: " + K);
	}

//Bai12
	public static void SoTanCungLa5(int[] a1, int N) {
		int H = 0;
		for (int i = 0; i < N; i++) {
			if (a1[i] % 5 == 0 && a1[i] % 2 != 0) {
				H++;
			}
		}
		System.out.println("So phan tu tan cung voi 5 la: " + H);
	}

//Ham main
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 0, x = 0, y = 0, m = 0;
		int N = NhapSoPhanTuMangNguyen(n);
		int M = NhapSoPhanTuMangThuc(m);
		int[] a1 = NhapMangNguyen(N);
		float[] b1 = NhapMangThuc(M);
		int x1 = NhapX(x);
		int y1 = NhapY(y, x1);
		SoAmMangThuc(b1, M);
		SoThucDoanXY(b1, M, x1, y1);
		SoNguyenChanDoanXY(a1, N, x1, y1);
		ASS4(a1, N);
		ASS5(a1, N);
		TongPhanTuMangNguyen(a1, N);
		TongPhanTuMangThuc(b1, M);
		TongCacSoNguyenLe(a1, N);
		TongCacSoHangChucLa5(a1, N);
		SoCacSoChan(a1, N);
		SoDuongChiaHet7(a1, N);
		SoPhanTuX(a1, N);
		SoTanCungLa5(a1, N);
	}
}
