package Asignment;

import java.util.*;

public class Assignment11 {

	public static int NhapN(int n) {
		Scanner input = new Scanner(System.in);
		System.out.println("nhập n: ");
		n = input.nextInt();
		System.out.println("-----------------");
		return n;
	}

//Bai1
	public static float[] NhapMangSoThuc(int P) {
		Scanner q = new Scanner(System.in);
		float[] a = new float[P];
		System.out.println("nhập a: ");
		for (int i = 0; i < P; i++) {
			a[i] = q.nextFloat();
		}
		System.out.println("-----------------");
		for (int i = 0; i < P; i++) {
			System.out.println("a[" + i + "]" + a[i]);
		}
		return a;
	}

//Bai2
	public static int[] NhapMangSoNguyen(int P) {
		Scanner q = new Scanner(System.in);
		int[] b = new int[P];
		System.out.println("nhập b: ");
		for (int i = 0; i < P; i++) {
			b[i] = q.nextInt();
		}
		System.out.println("-----------------");
		for (int i = 0; i < P; i++) {
			System.out.println("b[" + i + "]" + b[i]);
		}
		return b;
	}

//Bai3
	public static void XuatSoNguyenChan(int P, int[] b1) {
		System.out.println("-----------------");
		System.out.println("Số nguyên chẵn là:");
		for (int i = 0; i < P; i++) {
			if (b1[i] % 2 == 0) {
				System.out.println("b1[" + i + "]" + b1[i]);
			}
		}
	}

//Bai4
	public static void ViTriSoThucAm(int P, float[] a1) {
		System.out.println("-----------------");
		System.out.println("Vị trí số thực âm là:");
		for (int i = 0; i < P; i++) {
			if (a1[i] < 0) {
				System.out.println("a1[" + i + "]");
			}
		}
	}

//Bai5
	public static void SoThucLonNhat(int P, float[] a1) {
		System.out.println("-----------------");
		System.out.println("Số thực lớn nhất là: ");
		float max = Float.MIN_VALUE;
		for (int i = 0; i < P; i++) {
			max = (a1[i] > max) ? a1[i] : max;
		}
		System.out.println(max);
	}

//Bai6
	public static float SoThucDuongDauTien(int P, float[] a1) {
		for (int i = 0; i < P; i++) {
			if (a1[i] > 0.0) {
				return a1[i];
			}
		}
		return -1;
	}

//Bai7
	public static int SoNguyenChanCuoiCung(int P, int[] b1) {
		for (int i = P - 1; i > 0; i--) {
			if (b1[i] % 2 == 0) {
				return b1[i];
			}
		}
		return -1;
	}

//Bai8
	public static void ViTriThucNhoNhat(int P, float[] a1) {
		System.out.println("-----------------");
		System.out.println("Vị trí thực nhỏ nhất là:");
		float min = Float.MAX_VALUE;
		int i;
		for (i = 0; i < P; i++) {
			min = (a1[i] < min) ? a1[i] : min;
		}
		System.out.println("a[" + i + "]");

	}

//Bai9
	public static int ViTriSoNguyenChanDauTien(int P, int[] b1) {
		for (int i = 0; i < P; i++)
			if (b1[i] % 2 == 0)
				return i;
		return -1;
	}

//Bai10
	public static int SoHoanThien(int P) {
		int T = 0;
		for (int i = 1; i < P; i++)
			if (P % i == 0)
				T += 1;
		if (T == P)
			return 1;
		return 0;
	}

	public static int ViTriHoanThienCuoiCung(int P, int[] b1) {
		for (int i = P - 1; i > 0; i--) {
			if (SoHoanThien(b1[i] == 1)) {
				return i;
			}
		}
		return 0;
	}

	private static boolean SoHoanThien(boolean b) {
		// TODO Auto-generated method stub
		return false;
	}

//Bai11
	public static float SoThucDuongNhoNhat(int P, float[] a1) {
		float min1 = Float.MAX_VALUE;
		for (int i = 0; i < P; i++) {
			if (a1[i] > 0 && a1[i] < min1) {
				min1 = a1[i];
			}
		}
		return min1;
	}

//Bai12
	public static int ViTriDuongNhoNhatMangSoThuc(int P, float[] a1) {
		float min3 = Float.MAX_VALUE;
		for (int i = 0; i < P; i++) {
			if (a1[i] > 0 && a1[i] < min3) {
				return i;
			}
		}
		return -1;
	}

// Ham main
	public static void main(String[] agrs) {
		int n = 0;
		int P = NhapN(n);
		System.out.println("N = " + P);
		float[] a1 = NhapMangSoThuc(P);
		int[] b1 = NhapMangSoNguyen(P);
		XuatSoNguyenChan(P, b1);
		ViTriSoThucAm(P, a1);
		SoThucLonNhat(P, a1);
		System.out.println("-----------------");
		float A = SoThucDuongDauTien(P, a1);
		System.out.println("Số thực dương đầu tiên là " + A);
		System.out.println("-----------------");
		int B = SoNguyenChanCuoiCung(P, b1);
		System.out.println("Số nguyên chẵn cuối cùng là " + B);
		ViTriThucNhoNhat(P, a1);
		System.out.println("-----------------");
		int C = ViTriSoNguyenChanDauTien(P, b1);
		System.out.println("Vị trí số nguyên chẵn đầu tiên là: " + C);
		System.out.println("-----------------");
		int D = ViTriHoanThienCuoiCung(P, b1);
		System.out.println("Vị Trí hoang thiện cuối cùng là: " + D);
		System.out.println("-----------------");
		float E = SoThucDuongNhoNhat(P, a1);
		System.out.println("Số thực dương nhỏ nhất mảng sô thực là: " + E);
		System.out.println("-----------------");
		int F = ViTriDuongNhoNhatMangSoThuc(n, a1);
		System.out.println("Vị trí số thực dương nhỏ nhất mảng số thực là: " + F);
	}
}