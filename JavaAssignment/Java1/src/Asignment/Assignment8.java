package Asignment;

import java.util.*;

public class Assignment8 {

	public static String NhapChuoiX(String x) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhập chuỗi X:");
		x = input.nextLine();
		return x;
	}

	public static String NhapChuoiY(String y) {
		Scanner input = new Scanner(System.in);
		System.out.println("Nhập chuỗi Y:");
		y = input.nextLine();
		return y;
	}

	public static void Bai1(String x1, String y1) {
		System.out.println(x1.contains(y1));
	}

	public static void Bai2(String x1) {
		System.out.println("Các kí tự có trong chuỗi là: " + x1);
		String a = x1.toLowerCase();
		System.out.println("Các kí tự thường có trong chuỗi là: " + a);

	}

	public static void Bai3(String x1) {
		for (String w : x1.split("\\s", 0)) {
			System.out.println(w);
		}
	}

	public static void Bai4(String x1) {
		String b = new StringBuffer(x1).reverse().toString();

		System.out.println("Chuỗi bạn vừa nhập là : " + x1);
		System.out.println("Chuỗi ngược là : " + b);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String x = null, y = null;
		String x1 = NhapChuoiX(x);
		String y1 = NhapChuoiY(y);
		Bai1(x1, y1);
		Bai2(x1);
		Bai3(x1);
		Bai4(x1);
	}
}
