#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <ctime>

void NhapMang(int a[], int &n)
{
	scanf_s("%d", &n);
	for (int i = 0; i < n; i++)
	{
		printf("\nNhap a[%d]: ", i);
		scanf_s("%d", &a[i]);
	}
}
////Assignment6.1////
void XuatMang1(int a[], int n)
{
	printf("\nMang1 la:");
	for (int i = 0; i < n; i++)
	{
		printf("\na[%d]= %d",i, a[i]);
	}
}
////Assignment6.2////
void XuatMang2(int a[], int n)
{
	printf("\nMang2 la:");
	for (int i = 0; i < n; i++)
	{
		if (a[i] % 2 != 0)
		{
			printf("\n%d", i);
		}
	}
}


void NhapMangp2(int b[], int &k)
{
	srand(time(NULL));
	k = rand() % 100 + 0;
	for (int i = 0; i <k; i++)
	{
		b[i] = rand() % 100 + 0;
	}
}
////Assignment6.3////
void XuatMang3(int b[], int k)
{
	printf("\nMang 3 la:");
	int s = 0;
	for (int i = 0; i < k; i++)
	{
		printf("\nb[%d] = %d ", i, b[i]);
		s += b[i];
	}
	printf("\nKet qua la: %d", s);
}
////Assignment6.4////
void XuatMang4(int b[], int k)
{
	printf("\nMang 4 la:");
	int t = 0;
	for (int i = 0; i < k; i++)
	{
		if (b[i] % 2 != 0)
		{
			printf("\nb[%d] = %d", i, b[i]);
			t += b[i];
		}
	}
		printf("\nKet qua la: %d", t);
}
////Assignment6.5////
void XuatMang5(int a[], int n)
{
	printf("\nMang 5 la:");
		int p = 0;
		for (int i = 0; i < n; i++)
		{
			if (a[i] > 10)
			{
				p += a[i];
			}
		}
		printf("\n ket qua la: %d", p);
}

int main()
{
	int n;
	int a[100];
	NhapMang(a,n);
	XuatMang1(a,n);
	XuatMang2(a,n);
	XuatMang5(a, n);
	int k;
	int b[100];
	NhapMangp2(b, k);
	XuatMang3(b, k);
	XuatMang4(b, k);
	system("pause");
}	