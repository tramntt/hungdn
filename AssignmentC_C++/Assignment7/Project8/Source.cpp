#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <ctime>

void NhapMang(int a[], int &n)
{
	srand(time_t(NULL));
	scanf_s("%d", &n);
	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % (n + 1);
	}

}

void XuatMang(int a[], int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("\na[%d]= %d",i,a[i]);
	}
}

////Assignment7.1////
void SoLonNhat(int a[], int n)
{
	int max1 = a[0];
	for (int i = 0; i < n; i++)
	{
		if (a[i] > max1)
		{
			max1 = a[i];
		}
	}
	printf("\nGia tri lon nhat la: %d",max1);
}

////Assignment7.2////
void SoLe(int a[],int k[], int n)
{	
	for (int i = 0; i < n; i++)
	{
		if (a[i] % 2 != 0)
		{
			k[i] = a[i];
		}
	}
}

void SoLeLonNhat(int k[], int n)
{
	int max2 = k[0];
	for (int i = 0; i < n; i++)
	{
		if (k[i] > max2)
		{
			max2 = k[i];
		}
	}
	printf("\nSo le lon nhat la %d ", max2);
}

////Assignment7.3////
void NhapMang2(int b[], int n)
{
	for (int i = 0; i < n; i++)
	{
		b[i] = i;
	}
}

void XuatNguoc(int b[], int n)
{
	for (int i = (n-1); i>=0; i--)
	{
		printf("\nb[%d] = %d", i,b[i]);
	}
}

////Assignment7.4////
void GiaTriGiua(int a[], int n)
{
	int p;
	if (n % 2 != 0)
	{
		printf("\nSai yeu cau de");
	}
	else
	{
		for (int i = 1; i <= n; i++)
		{
			a[i] = i / 2;
			p = a[i];

		}
	}
	printf("\nPhan tu o giua la %d ", p);
}

int main()
{
	int a[100],k[100], b[100];
	int n;
	NhapMang(a, n);
	XuatMang(a, n);
	SoLonNhat(a, n);
	SoLe(a,k, n);
	SoLeLonNhat(k, n);
	NhapMang2(b, n);
	XuatNguoc(b,n);
	GiaTriGiua(a, n);
	system("pause");
}