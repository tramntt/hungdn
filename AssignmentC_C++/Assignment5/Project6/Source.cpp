#include <stdio.h>
#include <conio.h>
#include <iostream>

////Assignment5.1////
void TongDaySoThu1(float n)
{
	float T=0;
	for (float i = 2; i <= n; i++)
	{
		T += 1 / i;
	}
	printf("\nKet qua day 1 la: %0.4f", T);
}
////Assignment5.2////
void TongDaySoThu2(float n)
{
	float Q = 0;
		for (float i = 1; i < n; i++)
		{
			Q += i / (i + 1);
		}
	printf("\nKet qua day 2 la: %0.4f", Q);
}
////Assignment5.3////
void TongDaySoThu3(float n)
{
	float P = 0;
	float M = 0;
	float Z = 0;
	for (float i = 1; i <= n; i++)
	{
		P+= i/(i+1);
	}
	for(float i =4; i<=n;i+=2)
	{
		M+=(i-i)/i;
	}
	Z = P - M*2;
	printf("\nKet qua day 3 la: %0.4f", Z);
}
////Assignment5.4////
void TichDaySoThu4(float n)
{
	float R = 1;
	for (float i = 1; i <= n; i++)
	{
		R *= i;
	}
	printf("\nTich day so thu 4 la: %0.4f", R);
}
////Assignment5.5////
void TichDaySoThu5(float n)
{
	float E;
	for (float i = 1; i <= n; i++)
	{
		E = pow(n, i);
	}
	printf("\nTich day so thu 5 la: %f", E);
}
////Assignment5.6////
void TichDaySoThu6(float n)
{
	float W = 0;
	for (float i = 1; i <= n; i++)
	{
		W += (i*n);
	}
	printf("\nTich day so thu 6 la: %f", W);
}
////Assignment5.7////
void TichDaySoThu7(float n)
{
	float Y=0;
	for (float i = 1; i < n; i++)
	{
		Y += (i*(i + 1));
	}
	printf("\nTich day so thu 7 la: %f", Y);
}

void main()

{
	float n;
	scanf_s("%f", &n);
	TongDaySoThu1(n);
	TongDaySoThu2(n);
	TongDaySoThu3(n);
	TichDaySoThu4(n);
	TichDaySoThu5(n);
	TichDaySoThu6(n);
	TichDaySoThu7(n);
	system("pause");
}