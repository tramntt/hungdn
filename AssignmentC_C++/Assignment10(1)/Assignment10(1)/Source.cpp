#include <conio.h>
#include <math.h>
#include <stdio.h>

void Nhapn(int &n)
{
	printf("\nNhap vao n: ");
	scanf_s("%d", &n);
}

//Assignment10(1).1
void TongDay1(int n)
{
	int A = 0;
	for (int i = 0; i <= n; i++)
	{
		A = A + i;
	}
	printf("\nKet qua day 1 la: %d", A);
}

//Assignment10(1).2
void TongDay2(int n)
{
	int B = 0;
	for (int i = 1; i <= n; i++)
	{
		B+= pow(i, 2);
	}
	printf("\nKet qua day 2 la: %d", B);
}

//Assignment10(1).3
void TongDay3(int n)
{
	float C = 1;
	for (int i = 1; i <= n; i++)
	{
		C += i / (i++);
	}
	printf("\nKet qua day 3 la: %0.4f", C);
}

//Assignment10(1).4
void TongDay4(int n)
{
	float D = 0;
	for (int i = 1; i <= n; i++)
	{
		D += 1.0 / (i*2);
	}
	printf("\nKet qua day 4 la: %0.4f", D);
}

//Assignment10(1).5
void TongDay5(int n)
{
	float E = 1;
	for (int i = 1; i <= n; i += 2)
	{
		E += 1 / i;
	}
	printf("\nKet qua day 5 la: %0.4f", E);
}

//Assignmnet10(1).6
void TongDay6(int n)
{
	float F = 0;
	for (int i = 1; i <= n; i++)
	{
		F += 1.0 / (i*(i + 1));
	}
	printf("\nKet qua day 6 la: %0.4f", F);
}

//Assignment10(1).7
void TongDay7(int n)
{
	float G = 0;
	for (int i = 1; i <= n; i++)
	{
		G += (float)i / (i + 1);
	}
	printf("\nKet qua day 7 la: %0.4f", G);
}

//Assignment10(1).8
void TongDay8(int n)
{
	float H = 0;
	for (int i = 0; i <= n; i++)
	{
		H += (float)(2*i + 1) / (2*i + 2);
	}
	printf("\nKet qua day 8 la: %0.4f", H);
}

//Assignment10(1).9
void TichDay9(int n)
{
	double K = 1;
	for (int i = 1; i <= n; i++)
	{
		K *= i;
	}
	printf("\nKet qua day 9 la: %0.4f", K);
}

//Assignment10(1).10
void TichDay10(int n)
{
	int x;
	printf("\nNhap x: ");
	scanf_s("%d", &x);
	double T = pow(x, n);
	printf("\nKet qua day 10 la: %0.4f", T);
}

int main()
{
	int n;
	Nhapn(n);
	TongDay1(n);
	TongDay2(n);
	TongDay3(n);
	TongDay4(n);
	TongDay5(n);
	TongDay6(n);
	TongDay7(n);
	TongDay8(n);
	TichDay9(n);
	TichDay10(n);
	_getch();
	return 1;
}