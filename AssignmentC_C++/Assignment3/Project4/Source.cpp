﻿#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

void Nhap3So(int &a, int &b, int &c);
int Tich3So(int a, int b, int c);
int TongSoDauCuoi(int a, int c);
void Nhapn(int &n);
void TichDaySo(int n);
void TongSoChan(int n);
void Tong5(int n);

void man()
{
	int a, b, c, n;
	Nhap3So(a, b, c);
	int H = Tich3So(a, b, c);
	int G = TongSoDauCuoi(a, c);
	Nhapn(n);
	TichDaySo(n);
	TongSoChan(n);
	Tong5(n);
	printf("\nTich 3 so la %d", H);
	printf("\nTong so dau va cuoi la %d", G);

}

void Nhap3So(int &a, int &b, int &c)
{
	printf("\nNhap a ");
	scanf_s("%d", &a);

	printf("\nNhap b ");
	scanf_s("%d", &b);

	printf("\nNhap c ");
	scanf_s("%d", &c);
}
////Assignment3.1 hàm tính tích 3 số///
int Tich3So(int a, int b, int c)
{
	int A = a * b*c;
	return A;
}
////Assignment3.2 hàm tính tổng số đầu và cuối////
int TongSoDauCuoi(int a, int c)
{
	int B = a + c;
	return B;
}
////Assignment3.3 hàm tính tích dãy số n////
void Nhapn(int &n)
{
	do
	{
		printf("\nNhap n ");
		scanf_s("%d", &n);
	} while (n > 100);
}

void TichDaySo(int n)
{
	int T = 0;
	for (int i = 0; i < n; i++)
	{
		T = T * i;
	}
	printf("\nTich cac so la: %d", T);
}
////Assignment3.4 hàm tính tổng số chẵn trong dãy n////
void TongSoChan(int n)
{
	int Q = 0;
	for (int i = 0; i < n; i++)
	{
		if (i % 2 == 0)
			Q = Q + i;
	}
	printf("/nTong cac so chan la %d", Q);
}
////Assignment3.5 hàm tính tổng các sô chia hết cho 5 trong dãy n////
void Tong5(int n)
{
	int P = 0;
	for (int i = 0; i < n; i++)
	{
		if (i % 5 == 0)
			P = P + i;
	}
	printf("/nTong cac so chia het cho 5 la %d", P);
}

