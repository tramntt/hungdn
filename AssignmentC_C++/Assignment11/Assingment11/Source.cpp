#include <conio.h>
#include <stdio.h>
#include <iostream>

//Assignment11.1
void NhapMangSoThuc(int &n, float a[])
{
	printf("\nNhap n: ");
	scanf_s("%d", &n);
	for (int i = 0; i < n; i++)
	{
		printf("\nNhap a: ");
		scanf_s("\n%f", &a[i]);
	}
}

void XuatMangSoThuc(int n, float a[])
{
	for (int i = 0; i < n; i++)
	{
		printf("\na[%d] = %0.4f", i, a[i]);
	}
}

//Assignment11.2
void NhapMangSoNguyen(int &m, int b[])
{
	printf("\nNhap m: ");
	scanf_s("\n%d", &m);
	for (int i = 0; i < m; i++)
	{
		printf("\nNhap b: ");
		scanf_s("\n%d", &b[i]);
	}
}

void XuatMangSoNguyen(int m, int b[])
{
	for (int i = 0; i < m; i++)
	{
		printf("\nb[%d] = %d", i, b[i]);
	}
}

//Assingmnent11.3
void SoChanMangSoNguyen(int m, int b[])
{
	printf("\nGia tri chan trong day so nguyen la: ");
	for (int i = 0; i < m; i++)
	{
		if (b[i] % 2 == 0)
		{
			printf("\nb[%d] = %d", i, b[i]);
		}
	}
}

//Assignment11.4
void ViTriAmMangSoThuc(int n, float a[])
{
	printf("\nVi tri mang gia tri am trong mang so Thuc la:");
	for (int i = 0; i < n; i++)
	{
		if (a[i] < 0)
		{
			printf("\na[%d]", i);
		}
	}
}

//Assingment11.5
void SoLonNhatMangSoThuc(int n, float a[])
{
	printf("\nGia tri lon nhat mang so thuc la: ");
	float max =a[0];
	for (int i = 0; i < n; i++)
	{
		max = (max < a[i]) ? a[i] : max;
	}
	printf("\n%0.4f", max);
}

//Assignment11.6
int SoDuongDauTienMangSoThuc(int n, float a[])
{
	for (int i = 0; i < n; i++)
	{
		if (a[i] > 0)
		{
			return a[i];
		}
	}
	return -1;
}

//Assignment11.7 
int SoChanCuoiCungMangSoNguyen(int m, int b[])
{
	for (int i = m - 1; i > 0; i--)
	{
		if (b[i] % 2 == 0)
		{
			return b[i];
		}
	}
	return -1;
}

//Assignment11.8
void ViTriNhoNhatMangSoThuc(int n, float a[])
{
	printf("\nVi nho nhat mang so thuc la: ");
	float min1 = a[0];
	for (int i = 0; i<n; i++)
	{
		min1 = a[i] < min1 ? a[i] : min1;
		printf("\na[%d]", i);
	}
}

//Assignment11.9
int ViTriChanDauTienMangSoNguyen(int m, int b[])
{
	for (int i = 0; i < m; i++)
	{
		if (b[i] % 2 == 0)
		{
			return i;
		}
	}
	return -1;
}

//Assignment11.10 
int SoHoanThien(int m)
{
	int Tong = 0;
	for (int i = 0; i < m; i++)
		if (m%i == 0)
			Tong += i;
	if (Tong == m)
		return 1;
	return 0;
}
int ViTriHoanThienCuoiCungMangSoNguyen(int m, int b[])
{
	for (int i = m - 1; i > 0; i--)
		if (SoHoanThien(b[i] == 1))
			return i;
	return -1;
}

//Assignment11.11 (note)
int SoDuongNhoNhatMangSoThuc(int n, float a[])
{
	float min2;
	float A=0
	for (int i = 0; i < n; i++)
	{
		if (a[i] > 0)
		{
			A++;
			min2 = i;
			break;
		}
	}
	if (A == 0)
		return -1;
	for (int i = 0; i < n; i++)
	{
		if (a[i] > 0 && a[i] < a[min2])
		{
			min2 = i;
		}
	}
	return 1;
}

//Assignment11.12 
int ViTriDuongNhoNhatMangSoThuc(int n, float a[])
{
	float min3 = a[0];
	for (int i = 0; i < n; i++)
	{
		if (a[i] > 0)
		{
			min3 = a[i] < min3;
			return i;
		}
	}
	return -1;
}

int main()
{
	int n, m;
	float a[100];
	int b[100];
	NhapMangSoThuc(n, a);
	XuatMangSoThuc(n, a);
	ViTriAmMangSoThuc(n, a);
	SoLonNhatMangSoThuc(n, a);
	int A = SoDuongDauTienMangSoThuc(n, a);
	printf("\nSo duong dau tien mang so thuc la: %d", A);
	ViTriNhoNhatMangSoThuc(n, a);
	int B = SoDuongNhoNhatMangSoThuc(n, a);
	printf("\nSo duong nho nhat mang so thuc la: %d", B);
	int C = ViTriDuongNhoNhatMangSoThuc(n, a);
	printf("\nVi tri duong nho nhat mang so thu la: %d", C);
	NhapMangSoNguyen(m, b);
	XuatMangSoNguyen(m, b);
	SoChanMangSoNguyen(m, b);
	int D = SoChanCuoiCungMangSoNguyen(m, b);
	printf("\nSo chan cuoi cung mang so nguyen la: %d", D);
	int E = ViTriChanDauTienMangSoNguyen(m, b);
	printf("\nVi tri chan dau tien mang so nguyen la: %d", E);
	int F = ViTriHoanThienCuoiCungMangSoNguyen(m, b);
	printf("\nVi tri hoan thien cuoi cung mang so nguyen la: %d", F);
	_getch();
	return 1;
}