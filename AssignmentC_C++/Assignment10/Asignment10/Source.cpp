#include <conio.h>
#include <stdio.h>
#include <math.h>

void Nhapn(int &n)
{
	printf("Nhap n ");
	scanf_s("%d", &n);
}

void Nhapx(int &x)
{
	printf("Nhap x ");
	scanf_s("%d", &x);
}

//Assignment10.1
void TongDay1(int n)
{
	int T = 1;
	for (int i = 0; i <= n; i++)
	{
		T += T * i;
	}
	printf("\nKet qua day 1 la: %d", T);
}

//Assignment10.2
void TongDay2(int n,int x)
{
	int Q = 0;
	for (int i = 1; i <= n; i++)
	{
		Q += pow(x, i);
	}
	printf("\nKet qua day 2 la: %d", Q);
}

//Assignment10.3
void TongDay3(int n, int x)
{
	int P = 0;
	for (int i = 2; i <= n; i += 2)
	{
		P += pow(x, i);
	}
	printf("\nKet qua day 3 la: %d", P);
}

//Assingment10.4
void TongDay4(int n, int x)
{
	int W = 0;
	for (int i = 1; i <= n; i += 2)
	{
		W += pow(x, i);
	}
	printf("\nKet qua day 4 la: %d", W);
}

//Assignment10.5
void TongDay5(int n)
{
	int R = 0;
	for (int i = 1; i <= n; i++)
	{
		R += 1 /(R+i);
	}
	printf("\nKet qua day 5 la: %d", R);
}

//Assignment10.6
void TongDay6(int n, int x)
{
	int E = 0;
	for (int i = 1; i <= n; i++)
	{
		E += pow(x, i) / (E + i);
	}
	printf("\nKet qua day 6 la: %d", E);
}

//Assignment10.7
void TongDay7(int n, int x)
{
	int U = 1;
	for (int i = 1; i <= n; i++)
	{
		U += pow(x, i) / (U*i);
	}
	printf("\nKet qua day 7 la: %d", U);
}

int main()
{
	int n, x;
	Nhapn(n);
	Nhapx(x);
	TongDay1(n);
	TongDay2(n, x);
	TongDay3(n, x);
	TongDay4(n, x);
	TongDay5(n);
	TongDay6(n, x);
	TongDay7(n, x);
	_getch();
	return 1;
}