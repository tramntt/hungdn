#include <conio.h>
#include <stdio.h>
#include <iostream>

//Assignment12.1
void NhapMangSoThuc(int &n, float a[])
{
	printf("\nNhap n: ");
	scanf_s("%d", &n);
	for (int i = 0; i < n; i++)
	{
		printf("\nNhap a: ");
		scanf_s("\n%f", &a[i]);
	}
}

void XuatGiaTriAmMangSoThuc(int n, float a[])
{
	printf("\nCac gia tri am mang so thuc la: ");
	for (int i = 0; i < n;i++)
	{
		if (a[i] < 0)
		{
			printf("\na[%d] = %0.4f", i, a[i]);
		}
	}
}

//Asignment12.2
void NhapDoanxy(int &x, int &y)
{
	printf("\nNhap x: ");
	scanf_s("%d", &x);
	printf("\nNhap y: ");
	scanf_s("%d", &y);
}

void XuatCacSoDoanxyMST(int n, float a[], int x, int y)
{
	printf("\nCac so trong doan x,y mang so thuc la: ");
	for (int i = 0; i < n; i++)
	{
		if (x <= a[i] && a[i] <= y)
		{
			printf("\na[%d] = %0.4f", i, a[i]);
		}
	}
}

//Assignment12.3
void NhapMangSoNguyen(int &m, int b[100])
{
	printf("\nNhap m:");
	scanf_s("%d", &m);
	for (int i = 0; i < m; i++)
	{
		printf("\nNhap b: ");
		scanf_s("\n%d",&b[i]);
	}
}

void XuatCacSoDoanxyMSN(int m, int b[], int x, int y)
{
	printf("\nCac so thuoc doan x,y mang so nguen la: ");
	for (int i = 0; i < m; i++)
	{
		if (x <= b[i] && b[i] <= y)
		{
			printf("\nb[%d] = %d ", i, b[i]);
		}
	}
}

//Assignment12.4
void LietKeASS4(int m, int b[])
{
	printf("\nCac so thoa dk4 la: ");
	int A = 0;
	for (int i = 0; i < m; i++)
	{
		if (b[i] > abs(b[i + 1]))
		{
			A = 1;
			printf("%d", b[i]);
		}
	}
	if (A == 0)
	{
		printf("\nMang khong co gia tri");
	}
}

//Assignment12.5
void LietKeASS5(int m, int b[])
{
	printf("\nCac so thoa dk5 la: ");
	int B = 0;
	for (int i = 0; i < m; i++)
	{
		if (b[i] < abs(b[i + 1]) && b[i] > abs(b[i - 1]))
		{
			B = 1;
			printf("%d", b[i]);
		}
	}
	if (B == 0)
	printf("\nMang khong co gia tri");
}

//Assignment12.6
void TongCacGiaTriTrongMTN(int m, int b[])
{
	printf("\nKet qua tong gia tri trong MSN la: ");
	int C = 0;
	for (int i = 0; i < m; i ++ )
	{
		C += i;
	}
	printf("%d", C);
}

//Assignment12.7
void TongCacSoDuongMST(int n, float a[])
{
	printf("\nKet qua tong gia tri duong MST la: ");
	float D = 0;
	for (int i = 0; i < n; i++)
	{
		if (a[i] > 0)
		{
			D += a[i];
		}
	}
	printf("%f", D);
}

//Assignment12.8
int ChuSoLeDau(int m)
{
	int M;
	m = abs(m);
	while (m >= 10)
	{
		M = m % 10;
		m /= 10;
	}
	if (m % 2 == 0)
	{
		return 0;
	}
	return 1;
}
void TongCacSoBatDauBangSoLe(int m, int b[])
{
	printf("\nTong cac so bat dau bang so le: ");
	int E = 0;
	for (int i = 0; i < m; i++)
	{
		if (ChuSoLeDau(b[i]) == 1)
		{
			E += b[i];
		}
	}
	printf("%d", E);
}

//Assignment12.9
int SoHangChucKetThucVoi5(int m)
{
	m = abs(m);
	m = m / 10;
	int KTB5 = m % 10;
	if (KTB5 == 5)
	{
		return 1;
	}
	return 0;
}
void TongSoKetThucBang5(int m, int b[])
{
	int F = 0;
	for (int i = 0; i < m; i++)
	{
		if (SoHangChucKetThucVoi5(b[i]) == 1)
		{
			F += b[i];
		}
	}
	printf("\nTong cac so ket thuc voi 5 la: %d", F);
}

//Assignment12.10
void SoLuongSoChanMSN(int m, int b[])
{
	int G=0;
	for (int i = 0; i < m; i++)
	{
		if (b[i] % 2 == 0)
		{
			G++;
		}
	}
	printf("\nSo luong so chan trong mang la: %d", G);
}
void SoDuongChiaHetCho7(int m, int b[])
{
	int H=0;
	for (int i = 0; i < m; i++)
	{
		if (b[i] > 0 && b[i] % 7 == 0)
		{
			H++;
		}
	}
	printf("\nSo luong so duong chia he cho 7 la: %d", H);
}

//Assignment12.11
void SoLanxXuatHien(int m, int b[])
{
	int K = 0;
	for (int i = 0; i < m; i++)
	{
		if (b[i] == 'x')
		{
			K++;
		}
	}
	printf("\nSo lan xuat hien cua x la: ", K);
}

//Assignmnent12.12
void SoLuongGiaTriTanCungLa5(int m, int b[])
{
	int Q = 0;
	for (int i = 0; i < m; i++)
	{
		if (b[i] % 10 == 5)
		{
			Q++;
		}
	}
	printf("\nSo luong gia tri tan cung bang 5 la: ", Q);
}

int main()
{
	int n,m,b[100]; float a[100];
	int x, y;
	NhapMangSoThuc(n, a);
	XuatGiaTriAmMangSoThuc(n, a);
	NhapDoanxy(x, y);
	XuatCacSoDoanxyMST(n, a, x, y);
	NhapMangSoNguyen(m, b);
	XuatCacSoDoanxyMSN(m, b, x, y);
	LietKeASS4(m, b);
	LietKeASS5(m, b);
	TongCacGiaTriTrongMTN(m, b);
	TongCacSoDuongMST(n, a);
	TongCacSoBatDauBangSoLe(m, b);
	TongSoKetThucBang5(m, b);
	SoLuongSoChanMSN(m, b);
	SoDuongChiaHetCho7(m, b);
	SoLanxXuatHien(m, b);
	SoLuongGiaTriTanCungLa5(m, b);
	_getch();
	return 1;
}